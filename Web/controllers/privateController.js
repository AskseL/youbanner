var express = require('express');
var router = express.Router();
var google = require('googleapis');
var OAuth2 = google.auth.OAuth2;
var path = require('path');
var Banner = require('../models/banner');

//Check Auth middleware
exports.checkAuth = function(req, res, next){
	if (req.isAuthenticated()) {
		res.locals.user = req.user;
		next();
	}
	else {
		res.redirect('/login');
	}
};

//Controller methods
exports.index = function(req, res){ res.render('index') };
exports.profile = function(req, res){ res.render('profile') };
exports.logout = function(req, res){ req.logout(); res.redirect('/') };
exports.banners = function(req, res, next){
	Banner.find({owner: req.user.id}, (err, banners)=>{
		err ? next(err) : res.render('banners', {banners: banners})
	})
};

exports.bannersAdd = function(req, res, next){ res.render('bannersAdd') };
exports.bannersAddPOST = function(req, res, next){
	var banner = new Banner({
		name: req.body.banner_name,
		fabric_obj: req.body.fabric_obj,
		status: 0,
		owner: req.user.id
	});
	banner.save((err)=>{ err ? next(err) : res.redirect('/private/banners'); });
};
exports.bannersRun = (req, res)=>{
	Banner.findOne({owner: req.user.id, _id: req.body.id, status: {$nin: [1,2]}}, (err, banner)=>{
		if(err || !banner) res.status(500).send('500 BAD');
		else {
			Banner.find({owner: req.user.id, status: {$in: [1,2]}}, (err, activeBanners)=>{
				if(err) res.status(500).send('500 BAD'); else {
					if(activeBanners.length == 0){
						banner.status = 1;
						banner.save(err=>{
							if(err) res.status(500).send('500 BAD'); else res.status(200).send('OK');
						});
					} else res.status(403).send('403 BAD');
				}
			});
		}
	});
};
exports.bannersStop = (req, res)=>{
	Banner.findOne({owner: req.user.id, _id: req.body.id, status: {$nin: [-1,0]}}, (err, banner)=>{
		if(err || !banner) res.status(500).send('500 BAD');
		else {
			banner.status = 0;
			banner.save(err=>{
				if(err) res.status(500).send('500 BAD'); else res.status(200).send('OK');
			});
		}
	});
};
exports.bannersDel = (req, res)=>{
	Banner.findOne({owner: req.user.id, _id: req.body.id}, (err, banner)=>{
		if(err || !banner) res.status(500).send('500 BAD');
		else {
			banner.remove(err=>{
				err ? res.status(500).send('500 BAD') : res.status(200).send('OK');
			});
		}
	});
};

exports.refreshTokens = function(req, res, next){
	var User = require('../models/user');
	User.findOne({ googleID : req.user.googleID }, function(err, usr){
		if(err) next(err);
		usr.refreshAccessToken(usr, function(err, response){
			err ? next(err) : res.send(200, 'OK');
		});
	});
};
