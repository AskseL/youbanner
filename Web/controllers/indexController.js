var express = require('express');
var router = express.Router();
var passport = require('../config/youtubeStrategy');

//Controller methods

exports.index = function(req, res){ res.render('landing') };

exports.login = passport.authenticate('google');

exports.oauth2callback = passport.authenticate('google', { failureRedirect: '/login' });

exports.oauth2callbackRedirect = function(req, res) { res.redirect('/private'); };