var canvas = null;

//Backgroud load
$('#image').change((e)=>{
	var file = e.target.files[0];
	var reader = new FileReader();
  	reader.onload = function (f) {
    	var data = f.target.result;
    	fabric.Image.fromURL(data, function (img) {
			let width = $('#ccf').width();
			let height = width * 0.5625;
			$('#ccf').html("<canvas id=\"main_canvas\" width=\""+width+"\" height=\""+height+"\" style=\"border: 1px black solid;\"></canvas>");
			canvas = new fabric.Canvas('main_canvas');
			canvas.on('selection:cleared', hideAllSettings);
			canvas.setZoom(width / 2560);
      		img['selectable'] = false;
      		img['evented'] = false;
      		img.name = 'background';
      		canvas.add(img).renderAll();
    	});
	};
	reader.readAsDataURL(file);
});

//timecheckbox settings
$('#timeCheckBox').click(()=>{
	var existed_time = canvas.getObjects().filter((item)=>{
		//console.log(item);
		return item.name === 'time';
	});
	if($('#timeCheckBox').prop('checked')){
		var new_text = new fabric.Text('%time%');
		new_text.on('selected', (e)=>{ renderSettings('#time_settings') });
		new_text.fontFamily = 'Comic sans';
		new_text.scale(5);
		new_text.name = 'time';
		new_text.setColor('#ffffff');
		//Color picker callback
		$('#time_colorpicker').colorpicker().on('changeColor', e => {
			new_text.setColor(e.color.toHex());
			canvas.requestRenderAll();
		});
		//Font picker callback
		$('#time_font_picker').change((e)=>{
			new_text.fontFamily = e.target.value;
			canvas.requestRenderAll();
		});
		new_text['centeredScaling'] = true;
		canvas.add(new_text);
	} else{
		existed_time.forEach((item)=>{
			canvas.remove(item);
		});
	}
});

//countdown checkbox settings
$('#countDownTimerCheckBox').click(()=>{
	let existed_timer = canvas.getObjects().filter(item => { return item.name === 'countdown'; });
	if($('#countDownTimerCheckBox').prop('checked')){
		let new_timer = new fabric.Text('%CountDownTimer%');
		new_timer.on('selected', e => { renderSettings('#countdown_settings') });
		//Datetime picker initialize, set the callback on update
		$('#datetimepicker').datetimepicker().on('changeDate', function(ev){
			new_timer.toObject = (function(toObject) {
				return function(properties) {
			    	return fabric.util.object.extend(toObject.call(this, properties), {
			      		countdown: ev.date.valueOf(),
			    	});
			  	};
			})(new_timer.toObject);
		});
		//Color picker callback
		$('#countdown_colorpicker').colorpicker().on('changeColor', e => {
			new_timer.setColor(e.color.toHex());
			canvas.requestRenderAll();
		});
		//Font picker callback
		$('#countdown_font_picker').change((e)=>{
			new_timer.fontFamily = e.target.value;
			canvas.requestRenderAll();
		});

		new_timer.fontFamily = 'Comic sans';
		new_timer.scale(5);
		new_timer.name = 'countdown';
		new_timer.setColor('#ffffff');
		new_timer['centeredScaling'] = true;
		canvas.add(new_timer);
	} else {
		existed_timer.forEach((item)=>{
			canvas.remove(item);
		});
	}
});

//submit handler
$('#submit').click(function(){
	//Проверки заполнения полей
	if(!$('#banner_name').val()){
		let opt = {
			text: "Fill the banner's name field!",
			layout: "topRight",
			type: "error"
		}
		noty(opt);
	} else {
		$("#fabric_obj").val(JSON.stringify(canvas));
    	$("#frm").submit();
	}
});

//render widget settings by #id
function renderSettings(settings_group){
	$('.settings-group').addClass('hidden');
	$(settings_group).removeClass('hidden');
}

//hides all settings
function hideAllSettings(){
	$('.settings-group').addClass('hidden');
}

//Colorpicker initialize
$('.colorpicker-component').colorpicker({
	customClass: 'colorpicker-2x',
	sliders: {
		saturation: {
			maxLeft: 150,
			maxTop: 150
		},
		hue: {
			maxTop: 150
		},
		alpha: {
			maxTop: 150
		}
	}
});
