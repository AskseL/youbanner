function RunBanner(id) {
    $.ajax({
        type: "POST",
        url: "/private/banners/run",
        data: ({id: id}),
        success: function(data) {
            if(data === 'OK'){
                $('#'+id+' #status .label').attr('class', 'label').html('Waiting');
            }
        }
    });
}

function StopBanner(id) {
    $.ajax({
        type: "POST",
        url: "/private/banners/stop",
        data: ({id: id}),
        success: function(data) {
            $('#'+id+' #status .label').attr('class', 'label').html('Not active');
        }
    });
}

function DelBanner(id) {
    $.ajax({
        type: "POST",
        url: "/private/banners/del",
        data: ({id: id}),
        success: function(data) {
            if(data === 'OK'){
                $('#'+id).remove();
            }
        }
    });
}
