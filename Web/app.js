var express = require('express');
var path = require('path');
var favicon = require('serve-favicon');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var indexController = require('./controllers/indexController');
var privateController = require('./controllers/privateController');
var passport = require('./config/youtubeStrategy');
var fileUpload = require('express-fileupload');

var app = express();

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'twig');

//Middlewares
//app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));
app.use(require('./config/logger'));
app.use(bodyParser.json({limit: '50mb'}));
app.use(bodyParser.urlencoded({ extended: false, limit: '50mb' }));
app.use(cookieParser());
app.use(require('./config/session'));
app.use(passport.initialize());
app.use(passport.session());
app.use('/public', express.static(path.join(__dirname, 'public')));

//Index controllers
app.get('/', indexController.index);
app.get('/login', indexController.login);
app.get('/oauth2callback', indexController.oauth2callback, indexController.oauth2callbackRedirect);

//Private controllers
app.use('/private*', privateController.checkAuth);
app.get('/private', privateController.index);
app.get('/private/profile', privateController.profile);
app.get('/private/logout', privateController.logout);
app.get('/private/banners', privateController.banners);
app.get('/private/banners/add', privateController.bannersAdd);
app.post('/private/banners/add', fileUpload(), privateController.bannersAddPOST);
app.post('/private/banners/run', privateController.bannersRun);
app.post('/private/banners/stop', privateController.bannersStop);
app.post('/private/banners/del', privateController.bannersDel);


// catch 404 and forward to error handler
app.use(function(req, res, next) {
  var err = new Error('Not Found');
  err.status = 404;
  next(err);
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.extramessage = err.extramessage;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

module.exports = app;
