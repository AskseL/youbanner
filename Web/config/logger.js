var morgan = require('morgan');

module.exports = morgan(function (tokens, req, res) {
    if(!tokens.url(req, res).startsWith('/public')){
        return [
          '[' + new Date().toString().split(' ')[4] + ']',
          '['.concat(req.ip.split(':')[3], ']'),
          tokens.method(req, res),
          tokens.status(req, res),
          tokens.url(req, res),
        ].join(' ')
    }
});
