var passport = require('passport');
var GoogleStrategy = require('passport-youtube-v3').Strategy
var conf = require('../config');
var User = require('../models/user');

passport.use('google', new GoogleStrategy({
    clientID: conf.get('youtube:web:client_id'),
    clientSecret: conf.get('youtube:web:client_secret'),
    callbackURL: conf.get('youtube:web:redirect_uri'),
    scope:[
      'https://www.googleapis.com/auth/youtube.upload',
      'https://www.googleapis.com/auth/youtube',
      'https://www.googleapis.com/auth/youtube.force-ssl'
    ]
  },
  function(accessToken, refreshToken, profile, done) {
    User.findOne({googleID: profile.id}, function(err, user){
      if(err) return done(err);
      if(user) { return done(null, user); }

      var thumbnail = JSON.parse(profile._raw).items[0].snippet.thumbnails.default.url;
      var newUser = new User({
        username: profile.displayName,
        googleID: profile.id,
        accessToken: accessToken,
        refreshToken: refreshToken,
        thumbnail: thumbnail
      });
      newUser.save(function(err, saveres){
        if(err) return done(err);
        return done(null, newUser);
      });
    });
}));


passport.serializeUser(function(user, done) {
  done(null, user.googleID);
});

passport.deserializeUser(function(id, done) {
  User.findOne({googleID: id}, function(err, user) {
    done(err, user);
  });
});

module.exports = passport;
