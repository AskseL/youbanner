var session = require('express-session');
var MongoStore = require('connect-mongo')(session);

module.exports = session({
	secret: 'keyboardcat',
	resave: false,
	saveUninitialized: true,
	store: new MongoStore({ mongooseConnection: require('../models/connection').connection }),
	cookie: { maxAge: 24 * 60 * 60 * 1000 }
})