var mongoose = require('mongoose');
var config = require('../config');

mongoose.Promise = require('bluebird');

mongoose.connect(config.get('mongo_addr'), {
	useMongoClient: true,
});

module.exports = mongoose;