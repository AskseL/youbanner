var mongoose = require('./connection');

var BannerSchema = new mongoose.Schema({

  name: {
    type: String,
    required: true
  },
  fabric_obj: {
    type: String,
    required: true
  },
  status: {
  	type: Number,
  	required: true
  },
  owner: {
  	type: mongoose.Schema.Types.ObjectId,
  	ref: 'user',
  	required: true
  }

});


var Banner = mongoose.model('banner', BannerSchema);

module.exports = Banner;
