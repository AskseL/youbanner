var mongoose = require('./connection');

var UserSchema = new mongoose.Schema({

  username: {
    type: String,
    required: true
  },
  googleID: {
    type: String,
    required: true
  },
  accessToken: {
    type: String,
    required: true
  },
  refreshToken: {
    type: String,
    required: true
  },
  thumbnail: {
    type: String,
    required: true
  }

});


UserSchema.method('refreshAccessToken', function (user, callback){
  var conf = require('../config');
  var request = require('request');

  request.post({
    url:'https://www.googleapis.com/oauth2/v4/token',
    form: {
      client_id: conf.get('youtube:web:client_id'),
      client_secret: conf.get('youtube:web:client_secret'),
      refresh_token: user.refreshToken,
      grant_type: 'refresh_token'
    }
  },
  function(err,httpResponse,body){
    if(err) callback(err);
    newAccessToken = JSON.parse(body).access_token;
    if(newAccessToken && newAccessToken != ''){    
      user.accessToken = newAccessToken;
      user.save(callback);
    }
    else { callback(new Error()); }    
  })
});

var User = mongoose.model('user', UserSchema);

module.exports = User;