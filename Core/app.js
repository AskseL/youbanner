var Banner = require('./models/banner');
var User = require('./models/user');
var mongoose = require('./models/connection');
var fs = require('fs'),
    fabric = require('fabric').fabric

CoreEngineLoop();

function CoreEngineLoop(){
Banner.find({status: {$in: [1,2]}}, (err, banners)=>{
	if(err || banners.length == 0) { console.log(err); mongoose.disconnect(); return; }

	banners.forEach(banner=>{

    let out = fs.createWriteStream('/img/' + banner.owner + '.jpg');
	banner.status = 2;
	banner.save();
	var canvas = fabric.createCanvasForNode(2560, 1440);

	canvas.loadFromJSON(banner.fabric_obj, function() {
		var canvas_items = canvas.getObjects().forEach((item)=>{
			if(item.type === 'text' && item.text === '%time%'){
				item.text = new Date().toString().split(' ')[4];
			}
            if(item.type === 'text' && item.text === '%CountDownTimer%'){
                if(item.countdown){
                    item.text = new Date(item.countdown - Date.now()).toString().split(' ')[4];
                } else {
                    item.text = '';
                }
            }
		});

    	canvas.renderAll();
    	var stream = canvas.createJPEGStream();
		stream.on('data', function(chunk) {
			out.write(chunk);
		});
		stream.on('end', function(chunk) {
			User.findById(banner.owner, (err, user)=>{
				if(err) {console.log(err); return; }
				console.log('uploading...');

				let inst = fs.createReadStream('/img/' + banner.owner + '.jpg');

                //Загружаем баннер на канал
				Upload(inst, user, (err)=>{
                    //Ошибка при зарузке...
                    if(err){
                        //Если ошибка креденшиалов, то пробуем рефрешнуть токен
                        if(err.errors[0].message === 'Invalid Credentials')
                            user.refreshAccessToken(user, err=>{
                                //Не получилось рефрешнуть, выводим в консоль стэк
                                if(err) console.log(err); else{
                                    //Зарефрешили токен
                                    console.log('AccessToken has been refreshed!');
                                    mongoose.disconnect();
                                };
                            });
                        else { console.log(err); mongoose.disconnect(); }
                    } else {
    					console.log('updated');
    					mongoose.disconnect();
                    }
				});
			});
		});
	});
	});
});
};

function Upload(chunk, user, callback){

	var google = require('googleapis');
	var OAuth2 = google.auth.OAuth2;
	var oauth2Client = new OAuth2();

	oauth2Client.setCredentials({
	  	access_token: user.accessToken,
	  	refresh_token: user.refreshToken,
	});


	var youtube = google.youtube({
		version: 'v3'
	});

	youtube.channelBanners.insert({
		auth: oauth2Client,
		media: {
	        mimeType: "image/jpeg",
	        body: chunk
    	}
	},
	function (err, uploadResponse, insertResponse) {
        console.log('--insert');
		if(err) callback(err);
        else if (!uploadResponse) callback(new Error('insert')); else{
            youtube.channels.list({
    			auth: oauth2Client,
                part: "brandingSettings",
                mine: true
            }, function(err, channelListRsp, listResponse) {
                console.log('--list');
                if(err) callback(err);
                else if (channelListRsp && channelListRsp.items && channelListRsp.items.length > 0) {
                    // set the url
                    channelListRsp.items[0].brandingSettings.image.bannerExternalUrl = uploadResponse.url;
                    //update channel brandingSettings
                    youtube.channels.update({
                    	auth: oauth2Client,
                        part: "brandingSettings",
                        resource: channelListRsp.items[0]
                    }, function(err, channelUpdateResp, updateResponse) {
                        console.log('--update');
                        if(err) callback(err);
                        else if (channelUpdateResp) callback(null);
                        else callback(new Error('update'));
                    });
                } else callback(new Error('list'));
            });
        }
	});

};
